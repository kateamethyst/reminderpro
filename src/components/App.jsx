import React, {Component} from 'react';
import '../App.css';
import { connect } from 'react-redux';
import { addReminder, deleteReminder, clearReminders } from '../actions';
import moment from 'moment';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            dueDate: '',
        }
    }

    addReminder() {
      this.props.addReminder(this.state.text, this.state.dueDate);

    }

    deleteReminder(id) {
        this.props.deleteReminder(id);
    }

    renderReminders() {
        const { reminders } = this.props;
        return (
            reminders.map(reminder => {
                return (
                    <li className="list-group-item" key={ reminder.id }>
                        <p>{ reminder.text }</p>
                        <p> { moment(new Date(reminder.dueDate)).fromNow() }</p>
                        <span onClick={ event => this.deleteReminder(reminder.id)} className="list-item delete-button">&#x2715;</span>
                    </li>
                )
            })
        )
    }

    render() {
        console.log('this.props', this.props);
        return (
           <div className="App">
                <div className="title">
                    Reminder Pro
                </div>
                <div className="form-inline">
                    <div className="form-group">
                        <input
                            className="form-control"
                            placeholder="I have to ..."
                            onChange={event => this.setState({text: event.target.value})}
                        />
                        <input className="form-control" type="datetime-local" onChange={ event=> this.setState({dueDate: event.target.value})}/>
                    </div>
                    <button
                        type="button"
                        className="btn btn-success form-control"
                        onClick={() => this.addReminder()}
                    >
                        Add
                    </button>
                    <button
                        type="button"
                        className="btn btn-default form-control"
                        onClick={() => this.props.clearReminders()}
                    >
                        Clear Reminders
                    </button>
                </div>
                <ul className="list-group">
                    { this.renderReminders() }
                </ul>
           </div>
        )
    }
}

function mapsStateToProps(state) {
    return {
        reminders: state
    }
}

export default connect(mapsStateToProps, {addReminder, deleteReminder, clearReminders}) (App);